package test.com.api.service;

import com.api.controller.mapper.UserMapper;
import com.api.domain.entities.User;
import com.api.dto.response.UserResponseDTO;
import com.api.dto.wrapper.DataDTO;
import com.api.exceptions.ServiceException;
import com.api.exceptions.UserNotFoundException;
import com.api.persistence.UserRepository;
import com.api.service.UserService;
import com.api.service.implementation.UserServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService = new UserServiceImpl();

    @Before
    public void setUp() {
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void should_throw_service_exception_when_id_user_is_null(){
        expectedException.expect(ServiceException.class);
        userService.getUserById(null);
    }

    @Test
    public void should_throw_service_exception_when_name_user_is_null(){
        expectedException.expect(ServiceException.class);
        userService.getUserByName(null);
    }


    @Test
    public void should_throw_user_not_found_exception_when_user_not_exist_by_id(){
        expectedException.expect(UserNotFoundException.class);
        expectedException.expectMessage("User not found!");

        userService.getUserById(-1);//Never will exist negative IDs
    }

    @Test
    public void should_throw_user_not_found_exception_when_user_not_exist_by_name(){
        expectedException.expect(UserNotFoundException.class);
        expectedException.expectMessage("User not found!");

        userService.getUserByName("Bad name");
    }

    @Test
    public void should_be_ok_when_user_exist_by_id(){
        User user = new User(1, "Test");
        Optional<User> optional = Optional.of(user);
        DataDTO<UserResponseDTO> userToCompare = new DataDTO<UserResponseDTO>(UserMapper.makeUserResponseDTO(user));
        userToCompare.setStatus("200 OK");
        userToCompare.setCode("200");
        userToCompare.setMessage("OK");

        doReturn(optional).when(userRepository).findUserById(1);
        DataDTO<UserResponseDTO> userForCompare = userService.getUserById(1);

        assertThat(userToCompare).isEqualToComparingFieldByField(userForCompare);
    }

    @Test
    public void should_be_ok_when_user_exist_by_name(){
        User user = new User(1, "Test");
        Optional<User> optional = Optional.of(user);
        DataDTO<UserResponseDTO> userToCompare = new DataDTO<UserResponseDTO>(UserMapper.makeUserResponseDTO(user));
        userToCompare.setStatus("200 OK");
        userToCompare.setCode("200");
        userToCompare.setMessage("OK");

        doReturn(optional).when(userRepository).findUserByName("Test");
        DataDTO<UserResponseDTO> userForCompare = userService.getUserByName("Test");

        assertThat(userToCompare).isEqualToComparingFieldByField(userForCompare);
    }

}
