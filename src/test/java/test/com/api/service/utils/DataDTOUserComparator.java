package test.com.api.service.utils;

import com.api.dto.response.UserResponseDTO;
import com.api.dto.wrapper.DataDTO;

public class DataDTOUserComparator {
    public static boolean isTheSameUser(final DataDTO<UserResponseDTO> userToCompare, final DataDTO<UserResponseDTO> userForCompare){
        return (((UserResponseDTO)userToCompare.getData()).equals(((UserResponseDTO)userForCompare.getData())));
    }
}
