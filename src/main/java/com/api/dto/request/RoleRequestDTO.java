package com.api.dto.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleRequestDTO {
	private int id;
	private String name;

	private List<UserRequestDTO> users;

	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public List<UserRequestDTO> getUsers() {
		return users;
	}

	public void setUsers(final List<UserRequestDTO> users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "RoleDTO{" + "id=" + id + ", name='" + name + '\'' + '}';
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final RoleRequestDTO other = (RoleRequestDTO) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			return other.name == null;
		} else { return name.equals(other.name); }
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
}
