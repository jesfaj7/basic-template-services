package com.api.dto.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserRequestDTO {
	private int id;
	private String name;

	private List<RoleRequestDTO> roles;

	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public List<RoleRequestDTO> getRoles() {
		return roles;
	}

	public void setRoles(final List<RoleRequestDTO> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "UserDTO{" + "id=" + id + ", name='" + name + '\'' + '}';
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final UserRequestDTO other = (UserRequestDTO) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			return other.name == null;
		} else { return name.equals(other.name); }
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
}
