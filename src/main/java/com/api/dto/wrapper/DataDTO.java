package com.api.dto.wrapper;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataDTO<T> {
	private String status;
	private String code;
	private String message;
    private T data;

    public DataDTO() {
    }

    public DataDTO(T data) {
        this.data = data;
    }

    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}