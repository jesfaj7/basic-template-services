package com.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.api.util.Constants;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("UserService")
													  .apiInfo(getApiInfo()).select()
													  .paths(regex(".*" + Constants.API_NAME + "/.*"))
													  .build();
	}

	@Bean
	public UiConfiguration uiConfig() {
	    return UiConfigurationBuilder
	            .builder()
	            .operationsSorter(OperationsSorter.METHOD)
	            .build();
	}
	
	private ApiInfo getApiInfo() {
		return new ApiInfoBuilder().title("UserService")
								   .description("Service for Users")
								   .version("Version 1.0")
								   .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
								   .license("Apache 2.0")
								   .build();
	}
}
