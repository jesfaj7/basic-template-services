package com.api.service;

import com.api.dto.request.UserRequestDTO;
import com.api.dto.response.UserResponseDTO;
import com.api.dto.wrapper.DataDTO;

import java.util.List;

public interface UserService {
	DataDTO<List<UserResponseDTO>> getAllUsers();

	DataDTO<UserResponseDTO> getUserById(Integer id);

	DataDTO<UserResponseDTO> getUserByName(String name);

	DataDTO<UserResponseDTO> saveUser(UserRequestDTO userRequestDTO);

	DataDTO<UserResponseDTO> deleteUser(UserRequestDTO userRequestDTO);
}
