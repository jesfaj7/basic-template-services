package com.api.service.implementation;

import com.api.controller.mapper.RoleMapper;
import com.api.domain.entities.Role;
import com.api.domain.entities.User;
import com.api.dto.request.RoleRequestDTO;
import com.api.dto.request.UserRequestDTO;
import com.api.dto.response.RoleResponseDTO;
import com.api.dto.wrapper.DataDTO;
import com.api.exceptions.RoleNotFoundException;
import com.api.exceptions.ServiceException;
import com.api.persistence.RoleRepository;
import com.api.persistence.UserRepository;
import com.api.service.RoleService;
import com.api.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("roleService")
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	UserRepository userRepository;

	@SuppressWarnings("unchecked")
	@Override
	public DataDTO<List<RoleResponseDTO>> findAll() {
		final List<Role> roles = (List<Role>) roleRepository.findAll();
		return fillDataDTOWithStatusOK(new DataDTO<List<RoleResponseDTO>>(RoleMapper.makeRoleResponseDTOList(roles)));
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataDTO<RoleResponseDTO> findRoleById(final Integer id) {
		if (null == id) {
			throw new ServiceException();
		}

		Role role;
		role = roleRepository.findRoleById(id).orElse(null);
		if (null == role) {
			throw new RoleNotFoundException("Role not found!");
		}

		return fillDataDTOWithStatusOK(new DataDTO<RoleResponseDTO>(RoleMapper.makeRoleResponseDTO(role)));
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataDTO<RoleResponseDTO> findRoleByName(final String name) {
		if (null == name) {
			throw new ServiceException();
		}

		Role role;
		role = roleRepository.findRoleByName(name).orElse(null);
		if (null == role) {
			throw new RoleNotFoundException("Role not found!");
		}
		return fillDataDTOWithStatusOK(new DataDTO<RoleResponseDTO>(RoleMapper.makeRoleResponseDTO(role)));
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataDTO<RoleResponseDTO> saveRole(final RoleRequestDTO roleRequestDTO) {

		Role role = RoleMapper.makeRole(roleRequestDTO);

		final Iterable<User> iterable = getUsers(roleRequestDTO.getUsers());

		final List<User> users = new ArrayList<>();

		iterable.forEach(users::add);

		role.setUsers(users);
		role = roleRepository.save(role);

		final RoleResponseDTO roleResponseDTO = RoleMapper.makeRoleResponseDTO(role);

		return fillDataDTOWithStatusOK(new DataDTO<RoleResponseDTO>(roleResponseDTO));
	}

	private Iterable<User> getUsers(final List<UserRequestDTO> users) {
		return userRepository.findAllById(users.stream().map(UserRequestDTO::getId).collect(Collectors.toList()));
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataDTO<RoleResponseDTO> deleteRole(final RoleRequestDTO roleRequestDTO) {
		final Role role = RoleMapper.makeRole(roleRequestDTO);

		roleRepository.deleteById(role.getId());

		return fillDataDTOWithStatusOK(new DataDTO<RoleResponseDTO>(RoleMapper.makeRoleResponseDTO(role)));
	}

	@SuppressWarnings("rawtypes")
	private DataDTO fillDataDTOWithStatusOK(final DataDTO dataDTO) {
		dataDTO.setCode(Integer.toString(HttpStatus.OK.value()));
		dataDTO.setStatus(HttpStatus.OK.toString());
		dataDTO.setMessage(Constants.OK_REQUEST);
		return dataDTO;
	}

}
