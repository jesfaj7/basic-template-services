package com.api.service.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.api.controller.mapper.UserMapper;
import com.api.domain.entities.Role;
import com.api.domain.entities.User;
import com.api.dto.request.RoleRequestDTO;
import com.api.dto.request.UserRequestDTO;
import com.api.dto.response.UserResponseDTO;
import com.api.dto.wrapper.DataDTO;
import com.api.exceptions.ServiceException;
import com.api.exceptions.UserNotFoundException;
import com.api.persistence.RoleRepository;
import com.api.persistence.UserRepository;
import com.api.service.UserService;
import com.api.util.Constants;

@Service("userService")
public class UserServiceImpl implements UserService {
	private static final String USER_NOT_FOUND = "User not found!";

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@SuppressWarnings("unchecked")
	@Override
	public DataDTO<List<UserResponseDTO>> getAllUsers() {
		final List<User> users = (List<User>) userRepository.findAll();

		return fillDataDTOWithStatusOK(new DataDTO<List<UserResponseDTO>>(UserMapper.makeUserResponseDTOList(users)));
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataDTO<UserResponseDTO> getUserById(final Integer id) {
		if (null == id) {
			throw new ServiceException();
		}

		User user;

		user = userRepository.findUserById(id).orElseThrow(() -> new UserNotFoundException(USER_NOT_FOUND));

		return fillDataDTOWithStatusOK(new DataDTO<UserResponseDTO>(UserMapper.makeUserResponseDTO(user)));
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataDTO<UserResponseDTO> getUserByName(final String name) {
		if (null == name) {
			throw new ServiceException();
		}

		User user;

		user = userRepository.findUserByName(name).orElseThrow(() -> new UserNotFoundException(USER_NOT_FOUND));

		return fillDataDTOWithStatusOK(new DataDTO<UserResponseDTO>(UserMapper.makeUserResponseDTO(user)));
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataDTO<UserResponseDTO> saveUser(final UserRequestDTO userRequestDTO) {

		User user = UserMapper.makeUser(userRequestDTO);

		final Iterable<Role> iterable = getRoles(userRequestDTO.getRoles());

		final List<Role> roles = new ArrayList<>();

		iterable.forEach(roles::add);

		user.setRoles(roles);
		user = userRepository.save(user);

		final UserResponseDTO userResponseDTO = UserMapper.makeUserResponseDTO(user);

		return fillDataDTOWithStatusOK(new DataDTO<UserResponseDTO>(userResponseDTO));
	}

	private Iterable<Role> getRoles(final List<RoleRequestDTO> roles) {
		return roleRepository.findAllById(roles.stream().map(RoleRequestDTO::getId).collect(Collectors.toList()));
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataDTO<UserResponseDTO> deleteUser(final UserRequestDTO userRequestDTO) {

		final User user = UserMapper.makeUser(userRequestDTO);

		userRepository.deleteById(user.getId());

		return fillDataDTOWithStatusOK(new DataDTO<UserResponseDTO>(UserMapper.makeUserResponseDTO(user)));
	}

	@SuppressWarnings("rawtypes")
	private DataDTO fillDataDTOWithStatusOK(final DataDTO dataDTO) {
		dataDTO.setCode(Integer.toString(HttpStatus.OK.value()));
		dataDTO.setStatus(HttpStatus.OK.toString());
		dataDTO.setMessage(Constants.OK_REQUEST);
		return dataDTO;
	}
}
