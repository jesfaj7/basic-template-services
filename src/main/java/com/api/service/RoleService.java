package com.api.service;

import com.api.dto.request.RoleRequestDTO;
import com.api.dto.response.RoleResponseDTO;
import com.api.dto.wrapper.DataDTO;

import java.util.List;

public interface RoleService {
	DataDTO<List<RoleResponseDTO>> findAll();
	DataDTO<RoleResponseDTO> findRoleById(Integer id);
	DataDTO<RoleResponseDTO> findRoleByName(String name);
	DataDTO<RoleResponseDTO> saveRole(RoleRequestDTO roleDTO);
	DataDTO<RoleResponseDTO> deleteRole(RoleRequestDTO roleDTO);
}
