package com.api.controller.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.api.dto.error.ExceptionDTO;
import com.api.exceptions.*;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
public class GeneralControllerExceptionHandler {

	@ExceptionHandler(value = {ServiceException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionDTO handleServiceException(ServiceException e, HttpServletResponse response) {
        ExceptionDTO exceptionDTO = new ExceptionDTO();

        exceptionDTO.setCode(HttpStatus.BAD_REQUEST.toString());
        exceptionDTO.setStatus(Integer.toString(HttpStatus.BAD_REQUEST.value()) + "-Service");
        exceptionDTO.setMessage(e.getMessage());
        return exceptionDTO;
    }

    @ExceptionHandler(value = {UserNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ExceptionDTO handleUserNotFoundException(UserNotFoundException e, HttpServletResponse response) {
        ExceptionDTO exceptionDTO = new ExceptionDTO();

        exceptionDTO.setCode(HttpStatus.NOT_FOUND.toString());
        exceptionDTO.setStatus(Integer.toString(HttpStatus.NOT_FOUND.value()));
        exceptionDTO.setMessage(e.getMessage());
        return exceptionDTO;
    }

    @ExceptionHandler(value = {RoleNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ExceptionDTO handleRoleNotFoundException(RoleNotFoundException e, HttpServletResponse response) {
        ExceptionDTO exceptionDTO = new ExceptionDTO();

        exceptionDTO.setCode(HttpStatus.NOT_FOUND.toString());
        exceptionDTO.setStatus(Integer.toString(HttpStatus.NOT_FOUND.value()));
        exceptionDTO.setMessage(e.getMessage());
        return exceptionDTO;
    }
}
