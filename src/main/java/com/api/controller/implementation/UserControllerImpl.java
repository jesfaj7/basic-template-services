package com.api.controller.implementation;

import static com.api.util.Constants.BASE_URL_SERVICE;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.api.controller.UserController;
import com.api.dto.error.ExceptionDTO;
import com.api.dto.request.RoleRequestDTO;
import com.api.dto.request.UserRequestDTO;
import com.api.dto.response.UserResponseDTO;
import com.api.dto.wrapper.DataDTO;
import com.api.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "*", maxAge = 2000)
@RestController("userController")
@RequestMapping(value = BASE_URL_SERVICE)
public class UserControllerImpl implements UserController {

	@Autowired
	private UserService userService;

	@ApiOperation(value = "Returns all users")
	@ApiResponses({ @ApiResponse(code = 200, message = "Users found", response = RoleRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class),
			@ApiResponse(code = 404, message = "Users not found", response = ExceptionDTO.class), })
	@GetMapping("/users")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataDTO<List<UserResponseDTO>> findAllUsers() {
		return userService.getAllUsers();
	}

	@ApiOperation(value = "Returns a user by ID")
	@ApiParam(value = "Id User")
	@ApiResponses({ @ApiResponse(code = 200, message = "User found", response = UserRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class),
			@ApiResponse(code = 404, message = "User not found", response = ExceptionDTO.class), })
	@GetMapping("/users/{id}")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataDTO<UserResponseDTO> findUserById(@PathVariable(value = "id") final Integer id) {
		return userService.getUserById(id);
	}

	@ApiOperation(value = "Returns a user by NAME")
	@ApiParam(value = "Name User")
	@ApiResponses({ @ApiResponse(code = 200, message = "User found", response = UserRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class),
			@ApiResponse(code = 404, message = "User not found", response = ExceptionDTO.class), })
	@GetMapping("/users/name/{name}")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataDTO<UserResponseDTO> findUserByName(@PathVariable(value = "name") final String name) {
		return userService.getUserByName(name);
	}

	@ApiOperation(value = "Create a new user")
	@ApiParam(value = "User information")
	@ApiResponses({ @ApiResponse(code = 200, message = "User added", response = UserRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class), })
	@PostMapping(value = "/users", produces = "application/json", consumes = "application/json")
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@Override
	public DataDTO<UserResponseDTO> addUser(@Valid @RequestBody final UserRequestDTO userRequestDTO) {
		return userService.saveUser(userRequestDTO);
	}

	@ApiOperation(value = "Update an user")
	@ApiParam(value = "User information")
	@ApiResponses({ @ApiResponse(code = 200, message = "User updated", response = UserRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class), })
	@PutMapping(value = "/users", produces = "application/json", consumes = "application/json")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataDTO<UserResponseDTO> updateUser(@Valid @RequestBody final UserRequestDTO userRequestDTO) {
		return userService.saveUser(userRequestDTO);
	}

	@ApiOperation(value = "Delete an user")
	@ApiParam(value = "User information")
	@ApiResponses({ @ApiResponse(code = 200, message = "User deleted", response = UserRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class), })
	@DeleteMapping(value = "/users/{id}")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataDTO<UserResponseDTO> deleteUser(@PathVariable(value = "id") final Integer id) {
		final UserRequestDTO userRequestDTO = new UserRequestDTO();
		userRequestDTO.setId(id);
		return userService.deleteUser(userRequestDTO);
	}
}
