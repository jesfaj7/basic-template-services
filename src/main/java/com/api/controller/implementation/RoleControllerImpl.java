package com.api.controller.implementation;

import static com.api.util.Constants.BASE_URL_SERVICE;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.api.controller.RoleController;
import com.api.dto.error.ExceptionDTO;
import com.api.dto.request.RoleRequestDTO;
import com.api.dto.request.UserRequestDTO;
import com.api.dto.response.RoleResponseDTO;
import com.api.dto.wrapper.DataDTO;
import com.api.service.RoleService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "*", maxAge = 2000)
@RestController("roleController")
@RequestMapping(value = BASE_URL_SERVICE)
public class RoleControllerImpl implements RoleController {

	@Autowired
	private RoleService roleService;

	@ApiOperation(value = "Returns all roles")
	@ApiResponses({ @ApiResponse(code = 200, message = "Roles found", response = RoleRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class),
			@ApiResponse(code = 404, message = "Roles not found", response = ExceptionDTO.class), })
	@GetMapping("/roles")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataDTO<List<RoleResponseDTO>> findAllRoles() {
		return roleService.findAll();
	}

	@ApiOperation(value = "Returns a role by ID")
	@ApiParam(value = "Id Role")
	@ApiResponses({ @ApiResponse(code = 200, message = "Role found", response = RoleRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class),
			@ApiResponse(code = 404, message = "Role not found", response = ExceptionDTO.class), })
	@GetMapping("/roles/{id}")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataDTO<RoleResponseDTO> findRoleById(@PathVariable(value = "id") final Integer id) {
		return roleService.findRoleById(id);
	}

	@ApiOperation(value = "Returns a role by NAME")
	@ApiParam(value = "Name Role")
	@ApiResponses({ @ApiResponse(code = 200, message = "Role found", response = RoleRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class),
			@ApiResponse(code = 404, message = "Role not found", response = ExceptionDTO.class), })
	@GetMapping("/roles/name/{name}")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataDTO<RoleResponseDTO> findRoleByName(@PathVariable(value = "name") final String name) {
		return roleService.findRoleByName(name);
	}

	@ApiOperation(value = "Create a new role")
	@ApiParam(value = "Role information")
	@ApiResponses({ @ApiResponse(code = 200, message = "Role added", response = UserRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class), })
	@PostMapping(value = "/roles", produces = "application/json", consumes = "application/json")
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@Override
	public DataDTO<RoleResponseDTO> addRole(@Valid @RequestBody final RoleRequestDTO roleDTO) {
		return roleService.saveRole(roleDTO);
	}

	@ApiOperation(value = "Update a role")
	@ApiParam(value = "Role information")
	@ApiResponses({ @ApiResponse(code = 200, message = "Role updated", response = UserRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class), })
	@PutMapping(value = "/roles", produces = "application/json", consumes = "application/json")
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@Override
	public DataDTO<RoleResponseDTO> updateRole(@Valid @RequestBody final RoleRequestDTO roleDTO) {
		return roleService.saveRole(roleDTO);
	}

	@ApiOperation(value = "Delete an role")
	@ApiParam(value = "Role information")
	@ApiResponses({ @ApiResponse(code = 200, message = "Role deleted", response = RoleRequestDTO.class),
			@ApiResponse(code = 400, message = "General service error", response = ExceptionDTO.class), })
	@DeleteMapping(value = "/roles/{id}")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataDTO<RoleResponseDTO> deleteRole(@PathVariable(value = "id") final Integer id) {
		final RoleRequestDTO roleRequestDTO = new RoleRequestDTO();
		roleRequestDTO.setId(id);
		return roleService.deleteRole(roleRequestDTO);
	}

}
