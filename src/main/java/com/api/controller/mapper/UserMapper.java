package com.api.controller.mapper;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.api.domain.entities.Role;
import com.api.domain.entities.User;
import com.api.dto.request.UserRequestDTO;
import com.api.dto.response.UserResponseDTO;

public class UserMapper {

    public static User makeUser(UserRequestDTO userRequestDTO) {
    	User user = new User();

    	if(null != userRequestDTO) {
        	if(userRequestDTO.getId() == 0) {
        		user.setId(null);
        	}else {
        		user.setId(userRequestDTO.getId());
        	}

        	user.setName(userRequestDTO.getName());

             if(null != userRequestDTO.getRoles()) {
                user.setRoles(RoleMapper.makeRoleList(userRequestDTO.getRoles()));
            }
    	}
    	
        return user;
    }

    public static User makeUser(UserResponseDTO userResponseDTO) {
    	User user = new User();

    	if(null != userResponseDTO) {
        	if(userResponseDTO.getId() == 0) {
        		user.setId(null);
        	}else {
        		user.setId(userResponseDTO.getId());
        	}

        	user.setName(userResponseDTO.getName());

             if(null != userResponseDTO.getRoles()) {
                user.setRoles(userResponseDTO.getRoles().stream().map(Role::new).collect(Collectors.toList()));
            }
    	}
    	
        return user;
    }
    public static UserRequestDTO makeUserRequestDTO(User user) {
        UserRequestDTO userDTO = new UserRequestDTO();

        if(null != user) {
            userDTO.setId(user.getId());
            userDTO.setName(user.getName());
            if(null != user.getRoles()) {
            	userDTO.setRoles(RoleMapper.makeRoleRequestDTOList(user.getRoles()));
            }

        }
        return userDTO;
    }

    public static UserResponseDTO makeUserResponseDTO(User user) {
        UserResponseDTO userResponseDTO = new UserResponseDTO();

        if(null != user) {
        	userResponseDTO.setId(user.getId());
        	userResponseDTO.setName(user.getName());
            if(null != user.getRoles()) {
            	userResponseDTO.setRoles(user.getRoles().stream().map(Role::getName).collect(Collectors.toList()));
            }

        }
        return userResponseDTO;
    }

    public static List<User> makeUserList(Collection<UserRequestDTO> usersRequestDTO) {
        if (usersRequestDTO == null) {
            return Collections.emptyList();
        }
        return usersRequestDTO.stream()
                .map(UserMapper::makeUser)
                .collect(Collectors.toList());
    }

    public static List<UserRequestDTO> makeUserRequestDTOList(Collection<User> users) {
        return users.stream()
                .map(UserMapper::makeUserRequestDTO)
                .collect(Collectors.toList());
    }

    public static List<UserResponseDTO> makeUserResponseDTOList(Collection<User> users) {
        return users.stream()
                .map(UserMapper::makeUserResponseDTO)
                .collect(Collectors.toList());
    }

    private UserMapper() {
        throw new IllegalStateException("Utility class");
    }
}
