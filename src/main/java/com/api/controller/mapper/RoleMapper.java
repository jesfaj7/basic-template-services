package com.api.controller.mapper;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.api.domain.entities.Role;
import com.api.domain.entities.User;
import com.api.dto.request.RoleRequestDTO;
import com.api.dto.response.RoleResponseDTO;

public class RoleMapper {

    public static Role makeRole(RoleRequestDTO roleRequestDTO) {
    	Role role = new Role();

    	if(null != roleRequestDTO) {
        	if(roleRequestDTO.getId() == 0) {
        		role.setId(null);
        	}else {
        		role.setId(roleRequestDTO.getId());
        	}

        	role.setName(roleRequestDTO.getName());

             if(null != roleRequestDTO.getUsers()) {
                role.setUsers(UserMapper.makeUserList(roleRequestDTO.getUsers()));
            }
    	}
    	
        return role;
    }

    public static Role makeRole(RoleResponseDTO roleResponseDTO) {
    	Role role = new Role();

    	if(null != roleResponseDTO) {
        	if(roleResponseDTO.getId() == 0) {
        		role.setId(null);
        	}else {
        		role.setId(roleResponseDTO.getId());
        	}

        	role.setName(roleResponseDTO.getName());

             if(null != roleResponseDTO.getUsers()) {
                role.setUsers(roleResponseDTO.getUsers().stream().map(User::new).collect(Collectors.toList()));
            }
    	}
    	
        return role;
    }
    public static RoleRequestDTO makeRoleRequestDTO(Role role) {
        RoleRequestDTO roleDTO = new RoleRequestDTO();

        if(null != role) {
            roleDTO.setId(role.getId());
            roleDTO.setName(role.getName());
            if(null != role.getUsers()) {
            	roleDTO.setUsers(UserMapper.makeUserRequestDTOList(role.getUsers()));
            }

        }
        return roleDTO;
    }

    public static RoleResponseDTO makeRoleResponseDTO(Role role) {
        RoleResponseDTO roleResponseDTO = new RoleResponseDTO();

        if(null != role) {
        	roleResponseDTO.setId(role.getId());
        	roleResponseDTO.setName(role.getName());
            if(null != role.getUsers()) {
            	roleResponseDTO.setUsers(role.getUsers().stream().map(User::getId).collect(Collectors.toList()));
            }

        }
        return roleResponseDTO;
    }

    public static List<Role> makeRoleList(Collection<RoleRequestDTO> rolesRequestDTO) {
        if (rolesRequestDTO == null) {
            return Collections.emptyList();
        }
        return rolesRequestDTO.stream()
                .map(RoleMapper::makeRole)
                .collect(Collectors.toList());
    }

    public static List<RoleRequestDTO> makeRoleRequestDTOList(Collection<Role> roles) {
        return roles.stream()
                .map(RoleMapper::makeRoleRequestDTO)
                .collect(Collectors.toList());
    }

    public static List<RoleResponseDTO> makeRoleResponseDTOList(Collection<Role> roles) {
        return roles.stream()
                .map(RoleMapper::makeRoleResponseDTO)
                .collect(Collectors.toList());
    }

    private RoleMapper() {
        throw new IllegalStateException("Utility class");
    }
}
