package com.api.controller;

import java.util.List;

import com.api.dto.request.RoleRequestDTO;
import com.api.dto.response.RoleResponseDTO;
import com.api.dto.wrapper.DataDTO;

public interface RoleController {

	public DataDTO<List<RoleResponseDTO>> findAllRoles();

	public DataDTO<RoleResponseDTO> findRoleById(Integer id);

	public DataDTO<RoleResponseDTO> findRoleByName(String name);

	public DataDTO<RoleResponseDTO> addRole(RoleRequestDTO roleRequestDTO);

	public DataDTO<RoleResponseDTO> updateRole(RoleRequestDTO roleRequestDTO);

	public DataDTO<RoleResponseDTO> deleteRole(Integer id);

}
