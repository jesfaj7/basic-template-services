package com.api.controller;

import java.util.List;

import com.api.dto.request.UserRequestDTO;
import com.api.dto.response.UserResponseDTO;
import com.api.dto.wrapper.DataDTO;

public interface UserController {
	public DataDTO<List<UserResponseDTO>> findAllUsers();

	public DataDTO<UserResponseDTO> findUserById(Integer id);

	public DataDTO<UserResponseDTO> findUserByName(String name);

	public DataDTO<UserResponseDTO> addUser(UserRequestDTO userRequestDTO);

	public DataDTO<UserResponseDTO> updateUser(UserRequestDTO userRequestDTO);

	public DataDTO<UserResponseDTO> deleteUser(Integer id);
}
