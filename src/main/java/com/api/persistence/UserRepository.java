package com.api.persistence;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.domain.entities.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{

	Optional<User> findUserById(Integer id);
	Optional<User> findUserByName(String name);

}
