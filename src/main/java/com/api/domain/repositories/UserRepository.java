package com.api.domain.repositories;

import java.util.List;

import com.api.domain.entities.User;

public interface UserRepository {
	public List<User> findAllUsers();

	public List<User> findUserById(Integer id);

	public List<User> findUserByName(String name);

	public List<User> addUser(User user);

	public List<User> updateUser(User user);

	public List<User> deleteUserById(Integer id);

}
