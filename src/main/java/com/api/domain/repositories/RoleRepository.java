package com.api.domain.repositories;

import java.util.List;

import com.api.domain.entities.Role;

public interface RoleRepository {
	public List<Role> findAllRoles();

	public List<Role> findRoleById(Integer id);

	public List<Role> findRoleByName(String name);

	public List<Role> addRole(Role user);

	public List<Role> updateRole(Role user);

	public List<Role> deleteRole(Integer id);

}
