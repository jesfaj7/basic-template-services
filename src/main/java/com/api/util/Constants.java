package com.api.util;

public class Constants {
    public static final String API_NAME = "user-api";
    public static final String API_VERSION = "v1";
    public static final String BASE_URL_SERVICE = API_NAME + "/" + API_VERSION;
    
    public static final String OK_REQUEST = "OK";

    private Constants() {
        throw new IllegalStateException("Utility class");
    }
}
