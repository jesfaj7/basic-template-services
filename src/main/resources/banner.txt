${Ansi.GREEN}   .   ____          _            __ _ _
${Ansi.GREEN}  /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
${Ansi.GREEN} ( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
${Ansi.GREEN}  \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
${Ansi.GREEN}   '  |____| .__|_| |_|_| |_\__, | / / / /
${Ansi.GREEN}  =========|_|==============|___/=/_/_/_/
${Ansi.RED} :: Spring Boot${spring-boot.formatted-version} :: ${Ansi.DEFAULT}